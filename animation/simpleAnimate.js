const animateStepMargin = 10;

const getCurrentMarginOf = element =>
  parseFloat(window.getComputedStyle(element).marginLeft);

export function simpleAnimate(element) {
  const marginAnimateThreshold = Math.random() * (window.innerWidth - 50); // Разный для того, чтобы одновременно запущенные анимации не заканчивались одновременно
  const animateStep = () => {
    const isAnimationCompleted =
      getCurrentMarginOf(element) >= marginAnimateThreshold;
    if (isAnimationCompleted) {
      return;
    }
    const currentMargin = getCurrentMarginOf(element);

    element.style.marginLeft = `${currentMargin + animateStepMargin}px`;
    requestAnimationFrame(animateStep);
  };

  requestAnimationFrame(animateStep);
}
export function simpleAnimateWithCallBack(element) {

}
export function simpleAnimatePromisified(element) {

}
